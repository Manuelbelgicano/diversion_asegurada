#include<signal.h>
#include<stdio.h>
#include <stdlib.h>

int contador = 0;
// Handler de la señal
void handler(int sig) {
	if (contador<5) {
		sleep(1);
		printf("\nEstúpido, ¿acaso crees que puedes pararme?\n\n",sig);
		sleep(1);
		contador++;
	} else {
		exit(-1);
	}
}

int main() {
	struct sigaction my_sigaction;

	// Asignamos el handler
	sigemptyset(&my_sigaction.sa_mask);
	my_sigaction.sa_handler = handler;
	my_sigaction.sa_flags = 0;
	
	// El handler es para la señal CTRL+C
	if (sigaction(SIGINT,&my_sigaction,NULL)==-1)
		exit(0);

	while (1) {
		printf("Jaja tonto\n");	
	}
	return 0;
}
